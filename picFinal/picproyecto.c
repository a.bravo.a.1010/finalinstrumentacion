#include <16f877a.h>
#include <stdio.h>
#fuses XT, NOWDT, NOPROTECT
#use delay (clock=4M)
#use rs232 (baud=9600, parity=N, xmit=pin_c6, rcv=pin_c7, bits=8)

void leersensortemperatura ();
void leersensorluz ();
   long contador = 0;
   float valortemp;
   float valorluz;
   
   char dato;  // ***
/*   
#int_TIMER0    // **
void timer0_interruocion(){
   contador++;
      if (contador == 1000){
         leersensortemperatura();
         leersensorluz();      
      }
      set_timer0(255);

}*/

#int_rda    // ***
void rda_isr(){
   dato = getc();
   
   switch(dato){
      case '1': leersensortemperatura();
      break;
      case '2': leersensorluz();
      break;
   }
}

void main(){
   
      setup_timer_0(RTCC_INTERNAL | RTCC_DIV_256);
      //enable_interrupts(INT_TIMER0);  // **    
      enable_interrupts(GLOBAL);
      enable_interrupts(int_rda);   // ***
      set_timer0(255);
  
      while (true){
     
     }
}

void leersensortemperatura(){
   setup_adc_ports(all_analog);
   setup_adc(adc_clock_internal);
   set_adc_channel(0);
   delay_us(50);
   valortemp=((read_adc()*5.0)/1023);
   
   printf("%0.2f\r\n",valortemp);
   
}

void leersensorluz(){
   setup_adc_ports(all_analog);
   setup_adc(adc_clock_internal);
   set_adc_channel(1);
   delay_us(50);
   valorluz=(read_adc());
   
   printf("%0.2f\r\n",valorluz);
   
}


