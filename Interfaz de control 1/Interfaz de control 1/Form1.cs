﻿using System;
using System.Windows.Forms;

namespace Interfaz_de_control_1
{
    public partial class Form1 : Form
    {
        int flag = 0, time = 0, temperatura, luz;
        string lecturaT, lecturaL;
        public Form1()
        {
            InitializeComponent();
            Detener.Enabled = false;
            label1.Text = "Temperatura: ";
            chart1.Titles.Add("Temperatura");
            chart2.Titles.Add("Luz");

            try
            { 
                serialPort1.Open(); 
            }catch(Exception msg){ 
                MessageBox.Show(msg.ToString()); 
            }
        }

        private void Iniciar_Click(object sender, EventArgs e)
        {
            flag = 1;

            Iniciar.Enabled = false;
            Detener.Enabled = true;
            label1.Text = "Temperatura: ";
            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();
            progressBar1.Value = 0;
            progressBar2.Value = 0;
            pictureBox1.Visible = true;
            pictureBox2.Visible = false;
            time = 0;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void progressBar2_Click(object sender, EventArgs e)
        {
            progressBar2.Value = luz;

            if (luz <= 0) {
                pictureBox1.Visible = false;
                pictureBox2.Visible = true;
            } else {
                pictureBox1.Visible = true;
                pictureBox2.Visible = false;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if(flag == 1) {
                serialPort1.Write("2");
                time++;
                if (time == 10)
                {
                    time = 0;                   
                    chart2.Series[0].Points.Clear();
                }                
                chart2.Series["Luz"].Points.AddY(luz);
            }
        }

        private void Detener_Click(object sender, EventArgs e)
        {
            flag = 2;

            Iniciar.Enabled = true;
            Detener.Enabled = false;
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            progressBar1.Value = temperatura;

            if (temperatura <= 18) {
                label1.Text = "Temperatura: " + temperatura.ToString() + "°C (Frío)";
            }
            if (temperatura > 18 && temperatura < 30) {
                label1.Text = "Temperatura: " + temperatura.ToString() + "°C (Templado)";
            }
            if (temperatura >= 30) {
                label1.Text = "Temperatura: " + temperatura.ToString() + "°C (Caluroso)";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (flag == 1){
                serialPort1.Write("1");                
                time++;
                if (time == 10)
                {
                    time = 0;
                    chart1.Series[0].Points.Clear();                    
                }
                chart1.Series["Temperatura"].Points.AddY(temperatura);                
            }        
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if(flag == 1){
                lecturaT = serialPort1.ReadLine();
                double temp = Convert.ToDouble(lecturaT);
                temperatura = Convert.ToInt16(temp);

                Invoke(new EventHandler(progressBar1_Click));

                lecturaL = serialPort1.ReadLine();
                double lz = Convert.ToDouble(lecturaL);
                luz = Convert.ToInt16(lz);
                
                Invoke(new EventHandler(progressBar2_Click));
            }
        }
        //private void ProyectoTemp(object sender, EventArgs e){}
    }
}
